simple breakout for sharp memory displays

tested with

- LS013B7DH03 128x128 1.28" 141ppi 2 colours
- LS013B7DH05 144x168 1.26" 175ppi 2 colours
- LS011B7DH03 160x68  1.08"        2 colours
- LPM013M126  176x176 1.28"        8 colours


![front](./pics/3d-front.webp)

![front](./pics/3d-back.webp)

![front](./pics/pcb.webp)

